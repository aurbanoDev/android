package com.aurbano.myapplication.adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.Comentario;

import java.util.ArrayList;

/**
 * Created by adriu on 21/12/2015.
 */
public class ComentarioAdapter extends ArrayAdapter<Comentario> {

    private Activity context;
    private ArrayList<Comentario> datos;

    public ComentarioAdapter(Activity context, ArrayList<Comentario> datos) {
        super(context, R.layout.comentario_layout, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.comentario_layout, null);

        TextView titulo = (TextView) item.findViewById(R.id.etTituloComentario);
        titulo.setText(datos.get(position).getTitulo());

        TextView incidencia = (TextView) item.findViewById(R.id.etIncidenciaComentario);
        incidencia.setText(datos.get(position).getTexto());

        String texto = "";
        int puntuacion = datos.get(position).getPuntuacion();
        String[] valoraciones = context.getResources().getStringArray(R.array.valoraciones);

        switch (puntuacion) {
            case 1:
                texto = valoraciones[0];
                break;
            case 2:
                texto = valoraciones[1];
                break;
            case 3:
                texto = valoraciones[2];
                break;
            case 4:
                texto = valoraciones[3];
                break;
        }

        TextView valoracion = (TextView) item.findViewById(R.id.etValoracionComentario);
        valoracion.setText(texto);

        return item;
    }
}
