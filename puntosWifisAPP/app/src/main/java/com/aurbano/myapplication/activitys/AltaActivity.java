package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.aurbano.myapplication.R;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.aurbano.myapplication.utilidades.Constantes.SERVER_URL;

public class AltaActivity extends Activity {

    private final int RESULTADO_CARGA_IMAGEN = 1;

    private EditText etUsuario;
    private EditText etContrasena;
    private EditText etMail;
    private CheckBox cbCondiciones;
    private ImageButton imageButtonFoto;
    private Button btConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
        etMail = (EditText) findViewById(R.id.etMail);
        imageButtonFoto = (ImageButton) findViewById(R.id.imageButtonFoto);
        cbCondiciones = (CheckBox) findViewById(R.id.cbCondiciones);

        inicializarBoton();
        iniciarlizarImageButton();
    }

    private void inicializarBoton() {
        btConfirmar = (Button) findViewById(R.id.btAlta);
        btConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cbCondiciones.isChecked()) {
                    Toast.makeText(AltaActivity.this, R.string.combo_box, Toast.LENGTH_LONG).show();
                    return;
                }

                String usuario = String.valueOf(etUsuario.getText());
                String contrasena = String.valueOf(etContrasena.getText());
                String mail = String.valueOf(etMail.getText());

                nuevoUsuario(usuario, contrasena, mail);
            }
        });
    }

    private void iniciarlizarImageButton() {
        imageButtonFoto = (ImageButton) findViewById(R.id.imageButtonFoto);
        imageButtonFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);

                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
            }
        });
    }

    private void nuevoUsuario(String usuario, String contrasena, String mail) {
        WebService service = new WebService();
        service.execute(usuario, contrasena, mail);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == RESULTADO_CARGA_IMAGEN) && (resultCode == RESULT_OK) && (data != null)) {
            Uri imagenSeleccionada = data.getData();
            String [] ruta = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(imagenSeleccionada, ruta, null, null, null);
            cursor.moveToFirst();

            int indice = cursor.getColumnIndex(ruta[0]);
            String picturePath = cursor.getString(indice);
            cursor.close();

            imageButtonFoto.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            template.getForObject(SERVER_URL + "/add_usuario?login=" + params[0] + "&password="
                    + params[1] + "&email=" + params[2], Void.class);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(AltaActivity.this, R.string.usuario_creado, Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
