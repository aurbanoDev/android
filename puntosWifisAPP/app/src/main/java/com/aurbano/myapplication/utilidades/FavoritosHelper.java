package com.aurbano.myapplication.utilidades;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by adriu on 20/12/2015.
 */
public class FavoritosHelper extends SQLiteOpenHelper {

    private final String TABLA_FAVORITOS = "CREATE TABLE favoritos (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "titulo TEXT, cobertura TEXT, estado TEXT, tipo TEXT, posicion TEXT, votos INTEGER, valoracion INTEGER)";


    public FavoritosHelper(Context contexto, String nombre, CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_FAVORITOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS favoritos");
        db.execSQL(TABLA_FAVORITOS);
    }
}
