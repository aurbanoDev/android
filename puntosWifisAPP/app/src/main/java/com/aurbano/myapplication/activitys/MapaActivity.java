package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.PuntoWifi;
import com.aurbano.myapplication.utilidades.Constantes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapaActivity extends Activity {

    private GoogleMap mapa;
    private Marker marker;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        MapsInitializer.initialize(this);

        mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mapa.setMyLocationEnabled(true);

        String accion = getIntent().getStringExtra("accion");
        if (accion.equals("puntoWifi")) {
            PuntoWifi punto = getIntent().getParcelableExtra("puntoWifi");
            if (punto != null)
               marcarPunto(punto);
        }

        if (accion.equals("puntosWifi")) {
            ArrayList<PuntoWifi> listadoWifis = getIntent().getParcelableArrayListExtra("puntosWifi");
            if (listadoWifis != null)
                marcarPuntosWifi(listadoWifis);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mapa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void marcarPunto(PuntoWifi punto) {

        mapa.addMarker(new MarkerOptions().position(punto.getCoordenadas()).title(punto.getTitulo()));
        CameraUpdate camara = CameraUpdateFactory.newLatLng(punto.getCoordenadas());
        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(12.0f), 2000, null);
    }

    private void marcarPuntosWifi(ArrayList<PuntoWifi> listaWifi) {

        if (listaWifi.size() > 0) {
            for (PuntoWifi puntoWifi : listaWifi)
                marcarPunto(puntoWifi);
        }

        CameraUpdate camara = CameraUpdateFactory.newLatLng(Constantes.ZARAGOZA);
        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(12.0f), 2000, null);
    }
}
