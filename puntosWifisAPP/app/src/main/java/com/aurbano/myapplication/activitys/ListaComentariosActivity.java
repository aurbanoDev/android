package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.adaptadores.ComentarioAdapter;
import com.aurbano.myapplication.entidades.Comentario;
import com.aurbano.myapplication.entidades.ListaComentarios;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static com.aurbano.myapplication.utilidades.Constantes.SERVER_URL;

public class ListaComentariosActivity extends Activity {

    private ComentarioAdapter adapter;
    private ListaComentarios comentarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        comentarios = new ListaComentarios();
        adapter = new ComentarioAdapter(this, comentarios);

        ListView lvComentarios = (ListView) findViewById(R.id.lvComentarios);
        lvComentarios.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        cargarOpiniones();
    }

    private void cargarOpiniones() {
        String titulo = getIntent().getStringExtra("titulo");
        System.out.println(titulo);
        WebService webService = new WebService();
        webService.execute(titulo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comentarios, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            comentarios = template.getForObject(SERVER_URL
                    + "/getComentarioW?titulo=" + params[0], ListaComentarios.class);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter.clear();
            for (Comentario comentario : comentarios) {
                adapter.add(comentario);
                System.out.println(comentario.getTitulo());
            }
        }
    }
}
