package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.Comentario;
import com.aurbano.myapplication.entidades.ListaComentarios;
import com.aurbano.myapplication.entidades.PuntoWifi;
import com.aurbano.myapplication.utilidades.FavoritosHelper;
import com.google.android.gms.maps.model.TileOverlay;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.aurbano.myapplication.utilidades.Constantes.CONTACTO;
import static com.aurbano.myapplication.utilidades.Constantes.ID_ALERTA;
import static com.aurbano.myapplication.utilidades.Constantes.SERVER_URL;

public class LoginActivity extends Activity {

    private EditText etUsuario;
    private EditText etContrasena;
    private Button btConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etContrasena = (EditText) findViewById(R.id.etContrasena);

        inicializarBoton();
    }

    private void inicializarBoton() {
        btConfirmar = (Button) findViewById(R.id.btConfirmar);
        btConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = String.valueOf(etUsuario.getText());
                String contrasena = String.valueOf(etContrasena.getText());

                comprobarLogin(usuario, contrasena);
                etContrasena.setText("");
            }
        });
    }

    private void comprobarLogin(String usuario, String contrasena) {
        WebService service = new WebService();
        service.execute(usuario, contrasena);
    }

    protected void mostrarAcercaDe() {
        AlertDialog.Builder  builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(R.string.acerca);
        builder.setMessage(R.string.texto_acerca);
        builder.setIcon(android.R.drawable.ic_menu_call);

        builder.setNegativeButton(R.string.sopote, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent llamada = new Intent(Intent.ACTION_CALL);
                llamada.setData(Uri.parse("tel:" + CONTACTO));
                startActivity(llamada);
            }
        });

        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            return true;

        if (id == R.id.menu_alta)
            startActivity(new Intent(this, AltaActivity.class));

        if (id == R.id.menu_acercade)
            mostrarAcercaDe();

        return super.onOptionsItemSelected(item);
    }

   private class WebService extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            Looper.prepare();
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            boolean login = false;
            try {
                 login = template.getForObject(SERVER_URL + "/comprobar_login?login="
                        + params[0] + "&password=" + params[1], Boolean.class);
            } catch (RuntimeException e) {
                Toast.makeText(LoginActivity.this, R.string.mensaje_error, Toast.LENGTH_SHORT).show();
            }

            return login;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean)
                startActivity(new Intent(LoginActivity.this, ListadoWifisActivity.class));

            if (!aBoolean)
                Toast.makeText(LoginActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
        }
    }
}
