package com.aurbano.myapplication.adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.PuntoWifi;

import java.util.ArrayList;

/**
 * Created by adriu on 16/12/2015.
 */
public class PuntoWifiAdapter extends ArrayAdapter<PuntoWifi> {

    private Activity context;
    private ArrayList<PuntoWifi> datos;

    public PuntoWifiAdapter(Activity context, ArrayList<PuntoWifi> datos) {
        super(context, R.layout.punto_wifi_layout, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.punto_wifi_layout, null);


        TextView titulo = (TextView) item.findViewById(R.id.tvTitulo);
        titulo.setText(datos.get(position).getTitulo());

        TextView cobertura = (TextView) item.findViewById(R.id.tvCobertura);
        cobertura.setText(datos.get(position).getCobertura());

        return item;
    }
}
