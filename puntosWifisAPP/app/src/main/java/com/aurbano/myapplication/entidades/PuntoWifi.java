package com.aurbano.myapplication.entidades;

import android.os.Parcel;
import android.os.Parcelable;

import com.aurbano.myapplication.utilidades.Util;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by adriu on 16/12/2015.
 */
public class PuntoWifi implements Parcelable {

    private String titulo;
    private String cobertura;
    private String tipo;
    private String estado;
    private String posicion;
    private LatLng coordenadas;

    private int cantidadVotos;
    private int valoracion;

    public PuntoWifi() {
        titulo = "";
        cobertura = "";
        tipo = "";
        estado = "";
        posicion ="";
    }

    public PuntoWifi(Parcel entrada) {
        titulo = entrada.readString();
        coordenadas = entrada.readParcelable(LatLng.class.getClassLoader());
    }

    public String getCobertura() {
        return cobertura;
    }

    public String getEstado() {
        return estado;
    }

    public String getTipo() {
        return tipo;
    }

    public String getTitulo() {
        if (titulo.length() > 25)
            return titulo.substring(0, 20) + "...";

        return titulo;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
        coordenadas = Util.parseCoordenadas(posicion);
    }

    public void setCobertura(String cobertura) {
        this.cobertura = cobertura;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getCantidadVotos() {
        return cantidadVotos;
    }

    public void setCantidadVotos(int cantidadVotos) {
        this.cantidadVotos = cantidadVotos;
    }

    public int getValoracion() {
        return valoracion;
    }

    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }

    public LatLng getCoordenadas() {
        return coordenadas;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeParcelable(getCoordenadas(), flags);
    }

    public static final Parcelable.Creator<PuntoWifi> CREATOR = new Parcelable.Creator<PuntoWifi>() {

        public PuntoWifi createFromParcel(Parcel in) {
            return new PuntoWifi(in);
        }

        public PuntoWifi[] newArray(int size) {
            return new PuntoWifi[size];
        }
    };
}
