package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.adaptadores.PuntoFavoritoAdapter;
import com.aurbano.myapplication.entidades.Comentario;
import com.aurbano.myapplication.entidades.ListaComentarios;
import com.aurbano.myapplication.entidades.PuntoWifi;
import com.aurbano.myapplication.utilidades.FavoritosHelper;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.aurbano.myapplication.utilidades.Constantes.ID_ALERTA;
import static com.aurbano.myapplication.utilidades.Constantes.SERVER_URL;

public class ListadoFavoritosActivity extends Activity implements
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener  {

    private ListView lvFavoritos;
    private PuntoFavoritoAdapter adapter;
    private ArrayList<PuntoWifi> datos;
    private PuntoWifi favoritoSeleccionado;

    private boolean servicioNotificaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_favoritos);

        datos = new ArrayList<PuntoWifi>();
        adapter = new PuntoFavoritoAdapter(this, datos);

        lvFavoritos = (ListView) findViewById(R.id.lvFavoritos);
        lvFavoritos.setAdapter(adapter);
        lvFavoritos.setOnItemClickListener(this);
        lvFavoritos.setOnItemLongClickListener(this);

        registerForContextMenu(lvFavoritos);
        if (servicioNotificaciones)
            return;

        iniciarServicioNotificaciones();
        servicioNotificaciones = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        refrescarFavoritos();
    }

    private void iniciarServicioNotificaciones() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new ServicioNotificaciones(), 0, 10, TimeUnit.SECONDS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listado_favoritos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            return true;

        if (id == R.id.menu_ver_todos_favoritos) {
            verTodosLosPuntosFavoritos();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void verTodosLosPuntosFavoritos() {
        Intent intent = new Intent(this, MapaActivity.class);
        intent.putExtra("accion", "puntosWifi");
        intent.putExtra("puntosWifi", datos);
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.ctx_favoritos, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ctx_borrar_favorito:
                borrarFavorito();
                refrescarFavoritos();
                return true;
            default:
                return false;
        }
    }

    private void borrarFavorito() {
        FavoritosHelper helper = new FavoritosHelper(this, "DBfavs", null , 1);
        SQLiteDatabase baseDatos = helper.getWritableDatabase();

        String[] args = new String[]{favoritoSeleccionado.getTitulo()};
        baseDatos.delete("favoritos", "titulo=?", args);
        baseDatos.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PuntoWifi puntoWifi = (PuntoWifi) parent.getAdapter().getItem(position);

        Bundle bundle = new Bundle();
        bundle.putString("titulo", puntoWifi.getTitulo());
        bundle.putString("cobertura", puntoWifi.getCobertura());
        bundle.putString("tipo", puntoWifi.getTipo());
        bundle.putString("estado", puntoWifi.getEstado());
        bundle.putString("posicion", puntoWifi.getPosicion());

        Intent intent = new Intent(this, DetallesWifiActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        favoritoSeleccionado = adapter.getItem(position);

        return false;
    }

    public void refrescarFavoritos() {
        WebService webService = new WebService();
        webService.execute();
    }

    private void lanzarServicio() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setContentTitle("Feedback")
                .setContentText("Hay nuevas Opiniones")
                .setTicker("alerta");

        Intent noIntent = new Intent(ListadoFavoritosActivity.this, ListadoFavoritosActivity.class);
        PendingIntent intent = PendingIntent.getActivity(ListadoFavoritosActivity.this, 0, noIntent, 0);
        builder.setContentIntent(intent);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(ID_ALERTA, builder.build());
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            FavoritosHelper helper = new FavoritosHelper(ListadoFavoritosActivity.this, "DBfavs", null , 1);
            SQLiteDatabase baseDatos = helper.getWritableDatabase();

            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            datos.clear();

            Cursor c = baseDatos.rawQuery("select * from favoritos", null);
            if (c.moveToFirst()) {
                do {
                    ListaComentarios lista = template.getForObject(SERVER_URL
                            + "/getComentarioW?titulo=" + c.getString(1), ListaComentarios.class);

                    int acumulado = 0, media = 0, recomendaciones = 0;
                    for (Comentario comentario : lista) {
                        if (comentario.isRecomendado())
                            recomendaciones++;

                        acumulado += comentario.getPuntuacion();
                    }

                    try {
                        media = acumulado / lista.size();
                    } catch(ArithmeticException e) {
                        media = 0;
                    }

                    PuntoWifi puntoWifi = new PuntoWifi();
                    puntoWifi.setTitulo(c.getString(1));
                    puntoWifi.setCobertura(c.getString(2));
                    puntoWifi.setEstado(c.getString(3));
                    puntoWifi.setTipo(c.getString(4));
                    puntoWifi.setPosicion(c.getString(5));
                    puntoWifi.setCantidadVotos(recomendaciones);
                    puntoWifi.setValoracion(media);
                    datos.add(puntoWifi);

                    ContentValues values = new ContentValues();
                    values.put("votos", recomendaciones);

                    String[] args = new String[1];
                    args[0] = puntoWifi.getTitulo();

                    baseDatos.update("favoritos", values, "titulo=?", args);
                } while(c.moveToNext());
            }

            baseDatos.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter.notifyDataSetChanged();
        }
    }

    private class ServicioNotificaciones implements Runnable {

        @Override
        public void run() {
            System.out.println("ejecutando");

            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            FavoritosHelper helper = new FavoritosHelper(ListadoFavoritosActivity.this, "DBfavs", null, 1);
            SQLiteDatabase baseDatos = helper.getWritableDatabase();

            Cursor c = baseDatos.rawQuery("select * from favoritos", null);
            if (!c.moveToFirst())
                return;

            do {
                ListaComentarios lista = template.getForObject(SERVER_URL
                        + "/getComentarioW?titulo=" + c.getString(1), ListaComentarios.class);

                int recomendaciones = 0;
                for (Comentario comentario : lista) {
                    if (comentario.isRecomendado())
                        recomendaciones++;
                }

                System.out.println("previo" + c.getInt(6) + " calculado" + recomendaciones);
                if (c.getInt(6) < recomendaciones) {
                    lanzarServicio();
                    return;
                }

            } while (c.moveToNext());

            baseDatos.close();
        }
    }
}
