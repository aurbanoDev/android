package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aurbano.myapplication.R;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.aurbano.myapplication.utilidades.Constantes.SERVER_URL;

public class OpinionActivity extends Activity {

    private Spinner cbValoracion;
    private ArrayAdapter<String> adapter;
    private Button  btConfirmarOpinion;
    private CheckBox cbRecomendado;
    private TextView tvTitulo;
    private EditText etIncidencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opinion);

        etIncidencia = (EditText) findViewById(R.id.etIncidencia);
        tvTitulo = (TextView) findViewById(R.id.etTituloIndidencia);
        tvTitulo.setText(getIntent().getStringExtra("titulo"));

        cbValoracion = (Spinner) findViewById(R.id.cbValoracion);
        cbRecomendado = (CheckBox) findViewById(R.id.cbRecomendado);

        String[] valoraciones = getResources().getStringArray(R.array.valoraciones);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, valoraciones);

        cbValoracion.setAdapter(adapter);
        inicializarBoton();
    }

    private void inicializarBoton() {
        btConfirmarOpinion = (Button) findViewById(R.id.btConfirmarOpinion);
        btConfirmarOpinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbValoracion.getSelectedItem() == null) {
                    Toast.makeText(OpinionActivity.this, R.string.mensaje_error_incidencia, Toast.LENGTH_SHORT).show();
                    return;
                }

                opinar();
                finish();
            }
        });
    }

    private void opinar() {
        String titulo = String.valueOf(tvTitulo.getText());
        String texto = String.valueOf(etIncidencia.getText());

        WebService webService = new WebService();
        webService.execute(titulo, texto);
    }

    private int getPuntacion() {
        String opcion = String.valueOf(cbValoracion.getSelectedItem());

        return adapter.getPosition(opcion) + 1;
    }

    private int getRecomendado() {
        return (cbRecomendado.isChecked()) ? 1 : 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_opinion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            template.getForObject(SERVER_URL + "/add_comentario?titulo=" + params[0]
                    + "&texto=" + params[1]
                    + "&puntuacion=" + getPuntacion()
                    + "&recomendado=" + getRecomendado()
                    + "&tipo=wifi"
                    , Void.class);

            return null;
        }
    }
}
