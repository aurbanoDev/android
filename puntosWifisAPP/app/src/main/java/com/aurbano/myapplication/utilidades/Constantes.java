package com.aurbano.myapplication.utilidades;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by adriu on 18/12/2015.
 */
public class  Constantes {

    public static final String SERVER_URL = "http://87.218.253.206:8080";
    public static final String XML_URL =
            "https://www.zaragoza.es/api/recurso/ciencia-tecnologia/punto-wifi.xml";

    public static final LatLng ZARAGOZA = new LatLng(41.652333, -0.886913);
    public static final String CONTACTO = "976363472";

    public static final int ID_ALERTA = 99;

}
