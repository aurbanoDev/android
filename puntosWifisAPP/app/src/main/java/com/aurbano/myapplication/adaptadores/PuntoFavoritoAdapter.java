package com.aurbano.myapplication.adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.PuntoWifi;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by adriu on 20/12/2015.
 */
public class PuntoFavoritoAdapter extends ArrayAdapter<PuntoWifi> {

    private Activity context;
    private ArrayList<PuntoWifi> datos;

    public PuntoFavoritoAdapter(Activity context, ArrayList<PuntoWifi> datos) {
        super(context, R.layout.punto_wifi_layout, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.punto_wifi_favorito_layout, null);

        TextView titulo = (TextView) item.findViewById(R.id.tvTituloFav);
        titulo.setText(datos.get(position).getTitulo());

        TextView cantidadVotos = (TextView) item.findViewById(R.id.tvCantidadVotos);
        cantidadVotos.setText(String.valueOf(datos.get(position).getCantidadVotos()));

        TextView valoracion = (TextView) item.findViewById(R.id.tvMedia);
        valoracion.setText(String.valueOf(datos.get(position).getValoracion()));

        return item;
    }
}
