package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.aurbano.myapplication.R;
import com.aurbano.myapplication.entidades.PuntoWifi;

public class DetallesWifiActivity extends Activity {

    private TextView etTitulo;
    private TextView etCobertura;
    private TextView etEstado;
    private TextView etTipo;

    private PuntoWifi puntoWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_wifi);

        etTitulo = (TextView) findViewById(R.id.etTitulo);
        etCobertura = (TextView) findViewById(R.id.etCobertura);
        etEstado = (TextView) findViewById(R.id.etEstado);
        etTipo = (TextView) findViewById(R.id.etTipo);

        recuperarExtras();
        cargarDetalles();
    }

    private void recuperarExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null)
            return;

        puntoWifi = new PuntoWifi();
        puntoWifi.setTitulo(bundle.getString("titulo"));
        puntoWifi.setPosicion(bundle.getString("posicion"));
        puntoWifi.setCobertura(bundle.getString("cobertura"));
        puntoWifi.setTipo(bundle.getString("tipo"));
        puntoWifi.setEstado(bundle.getString("estado"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detalles_wifi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            return true;

        if (id == R.id.menu_encontrar) {
            Intent intent = new Intent(this, MapaActivity.class);
            intent.putExtra("accion", "puntoWifi");
            intent.putExtra("puntoWifi", puntoWifi);
            startActivity(intent);
        }

        if (id == R.id.menu_crear_opinion) {
            Intent intent = new Intent(this, OpinionActivity.class);
            intent.putExtra("titulo", String.valueOf(etTitulo.getText()));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void cargarDetalles() {
        etTitulo.setText(puntoWifi.getTitulo());
        etCobertura.setText(puntoWifi.getCobertura());
        etTipo.setText(puntoWifi.getTipo());
        etEstado.setText(puntoWifi.getEstado());
    }
}
