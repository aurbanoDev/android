package com.aurbano.myapplication.utilidades;

import android.app.Dialog;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by adriu on 20/12/2015.
 */
public class Util {

    public static uk.me.jstott.jcoord.LatLng DeUMTSaLatLng(double este, double oeste, char zonaLat, int zonaLong) {

        uk.me.jstott.jcoord.UTMRef utm = new uk.me.jstott.jcoord.UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }

    public static LatLng parseCoordenadas(String localizacion) {

        String coordenadas[] = localizacion.split(",");

        uk.me.jstott.jcoord.LatLng ubicacion = Util.DeUMTSaLatLng(Double.parseDouble(coordenadas[0]),
                Double.parseDouble(coordenadas[1]), 'N', 30);

        return new LatLng(ubicacion.getLat(), ubicacion.getLng());
    }
}
