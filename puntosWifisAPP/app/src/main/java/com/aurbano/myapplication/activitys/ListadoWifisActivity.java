package com.aurbano.myapplication.activitys;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.aurbano.myapplication.entidades.PuntoWifi;
import com.aurbano.myapplication.adaptadores.PuntoWifiAdapter;
import com.aurbano.myapplication.R;
import com.aurbano.myapplication.utilidades.FavoritosHelper;
import com.aurbano.myapplication.utilidades.XMLparser;

import java.util.ArrayList;

import static com.aurbano.myapplication.utilidades.Constantes.ID_ALERTA;
import static com.aurbano.myapplication.utilidades.Constantes.XML_URL;

public class ListadoWifisActivity extends Activity implements
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ListView lvWifis;
    private PuntoWifiAdapter adapter;
    private PuntoWifi puntoSeleccionado;
    private ArrayList<PuntoWifi> listaPuntosWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_wifis);

        listaPuntosWifi = new ArrayList<PuntoWifi>();
        adapter = new PuntoWifiAdapter(ListadoWifisActivity.this, listaPuntosWifi);

        lvWifis = (ListView) findViewById(R.id.lvWifis);
        lvWifis.setAdapter(adapter);
        lvWifis.setOnItemClickListener(this);
        lvWifis.setOnItemLongClickListener(this);
        registerForContextMenu(lvWifis);
    }

    @Override
    protected void onResume() {
        super.onResume();

        cargarListaWifis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listado_wifis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            return true;

        if (id == R.id.menu_mis_favoritos)
            startActivity(new Intent(this, ListadoFavoritosActivity.class));

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.ctx_menu_listado, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ctx_como_llegar:
                System.out.println("como llegar");
                return true;
            case R.id.ctx_opinar:
                Intent intent = new Intent(this, OpinionActivity.class);
                intent.putExtra("titulo", puntoSeleccionado.getTitulo());
                startActivity(intent);
                return true;
            case R.id.ctx_ver_opiniones:
                Intent comentariosIntent = new Intent(this, ListaComentariosActivity.class);
                comentariosIntent.putExtra("titulo", puntoSeleccionado.getTitulo());
                startActivity(comentariosIntent);
                return true;
            case R.id.ctx_favoritos:
                agregarFavorito();
                return true;
            default:
                return false;
        }
    }

    private void cargarListaWifis() {
        TareaDescarga tareaDescarga = new TareaDescarga();
        tareaDescarga.execute(XML_URL);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PuntoWifi puntoWifi = (PuntoWifi) parent.getAdapter().getItem(position);

        Bundle bundle = new Bundle();
        bundle.putString("titulo", puntoWifi.getTitulo());
        bundle.putString("cobertura", puntoWifi.getCobertura());
        bundle.putString("tipo", puntoWifi.getTipo());
        bundle.putString("estado", puntoWifi.getEstado());
        bundle.putString("posicion", puntoWifi.getPosicion());

        Intent intent = new Intent(this, DetallesWifiActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void agregarFavorito() {
        FavoritosHelper helper = new FavoritosHelper(this, "DBfavs", null , 1);
        SQLiteDatabase baseDatos = helper.getWritableDatabase();

        if (baseDatos == null) {
            Toast.makeText(this, R.string.mensaje_error_bd, Toast.LENGTH_LONG).show();
            return;
        }

        String[] args = new String[]{puntoSeleccionado.getTitulo()};
        Cursor c = baseDatos.rawQuery("select * from favoritos where titulo=?", args);
        if (c.moveToFirst()) {
            baseDatos.close();
            return;
        }

        ContentValues registro = new ContentValues();
        registro.put("titulo", puntoSeleccionado.getTitulo());
        registro.put("cobertura", puntoSeleccionado.getCobertura());
        registro.put("estado", puntoSeleccionado.getEstado());
        registro.put("tipo", puntoSeleccionado.getTipo());
        registro.put("posicion", puntoSeleccionado.getPosicion());
        registro.put("votos", puntoSeleccionado.getCantidadVotos());
        registro.put("valoracion", puntoSeleccionado.getValoracion());

        baseDatos.insert("favoritos", null, registro);
        baseDatos.close();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        puntoSeleccionado = adapter.getItem(position);

        return false;
    }

    private class TareaDescarga extends AsyncTask<String, Void, Void> {

        private boolean error = false;
        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            XMLparser parser = new XMLparser(XML_URL);
            listaPuntosWifi = parser.parsear();

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            listaPuntosWifi = new ArrayList<PuntoWifi>();
        }

        @Override
        protected void onProgressUpdate(Void... progreso) {
            super.onProgressUpdate(progreso);

            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ListadoWifisActivity.this);
            dialog.setTitle(R.string.mensaje_cargando);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);

            if (error) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.mensaje_error), Toast.LENGTH_SHORT).show();
                return;
            }

            for (int i = 0; i < 40; i++) {
                if (listaPuntosWifi.get(i) == null)
                    break;

                adapter.add(listaPuntosWifi.get(i));
            }

            if (dialog != null)
                dialog.dismiss();
        }
    }
}
