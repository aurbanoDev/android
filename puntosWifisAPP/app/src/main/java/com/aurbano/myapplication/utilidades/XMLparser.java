package com.aurbano.myapplication.utilidades;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;

import com.aurbano.myapplication.entidades.PuntoWifi;
import com.google.android.gms.maps.model.LatLng;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by adriu on 19/12/2015.
 */
public class XMLparser {

    private URL url;
    private PuntoWifi puntoWifi;

    public XMLparser(String url) {
       this.url = getUrl(url);
    }

    public ArrayList<PuntoWifi> parsear() {
        final ArrayList<PuntoWifi> puntosWifis = new ArrayList<PuntoWifi>();

        RootElement root = new RootElement("resultado");
        Element channel = root.getChild("result");
        Element item = channel.getChild("punto-wifi");
        Element geometria = item.getChild("geometry");

        item.setStartElementListener(new StartElementListener() {
            @Override
            public void start(Attributes attributes) {
                puntoWifi = new PuntoWifi();
            }
        });

        item.setEndElementListener(new EndElementListener() {
            @Override
            public void end() {
                puntosWifis.add(puntoWifi);
            }
        });

        item.getChild("title").setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                puntoWifi.setTitulo(body);
            }
        });

        item.getChild("tipo").setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                puntoWifi.setTipo(body);
            }
        });

        item.getChild("estado").setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                puntoWifi.setEstado(body);
            }
        });

        item.getChild("cobertura").setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                puntoWifi.setCobertura(body);
            }
        });

        geometria.getChild("coordinates").setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                puntoWifi.setPosicion(body);
            }
        });


        try {
            Xml.parse(getInputStream(), Xml.Encoding.UTF_8, root.getContentHandler());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return puntosWifis;
    }

    private URL getUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private InputStream getInputStream() {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
